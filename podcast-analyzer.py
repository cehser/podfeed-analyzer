import feedparser
import csv

feed = feedparser.parse('http://minkorrekt.de/feed/mp3/')
cols = ['title', 'itunes_duration', 'published']

data = [ {k: feed['entries'][i][k] for k in cols} for i in range(0,len(feed['entries'])) ]

print (feed['entries'][90].keys())

with open('feed.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=cols, delimiter=';')
    writer.writeheader()
    for d in data:
        writer.writerow(d)